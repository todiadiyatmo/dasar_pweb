<?php

 class kalkulator_mutator{
	private $merk,$seri,$pemilik;

	public function penjumlahan ($a,$b){
		return $a+$b;
	}

	/**
	 * Getter for pemilik
	 *
	 * @return mixed
	 */
	public function getPemilik()
	{
	    return $this->pemilik;
	}
	
	/**
	 * Setter for pemilik
	 *
	 * @param mixed $pemilik Value to set
	
	 * @return self
	 */
	public function setPemilik($pemilik)
	{
	    $this->pemilik = $pemilik;
	    return $this;
	}
	
	/**
	 * Getter for seri
	 *
	 * @return mixed
	 */
	public function getSeri()
	{
	    return $this->seri;
	}
	
	/**
	 * Setter for seri
	 *
	 * @param mixed $seri Value to set
	
	 * @return self
	 */
	public function setSeri($seri)
	{
	    $this->seri = $seri;
	    return $this;
	}


	/**
	 * Getter for merk
	 *
	 * @return mixed
	 */
	public function getMerk()
	{
	
	    echo $this->merk;
	}
	
	/**
	 * Setter for merk
	 *
	 * @param mixed $merk Value to set
	
	 * @return self
	 */
	public function setMerk($merk)
	{
	    $this->merk = $merk;
	    return $this;
	}
	
	
}

