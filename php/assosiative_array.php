<?php
// Deklarasi array associative, kumpulan motor dan pemiliknya
// kumpulan dari key => value

$kumpulan_motor = array(
	"Adnan"=>"Honda Beat 2012",
	"Yafie"=>"Honda Revo 2009",
	"Ekky"=>"Yamaha Mio 2010",
	);

//melihat isi array , var_dump merupakan fungsi php yang berguna "membongkar isi variabel"

var_dump($kumpulan_motor);

//mengambil isi array satu per satu, data diambil berdasar 'key'

echo "<br> Motor pertama {$kumpulan_motor['Adnan']}";
echo "<br> Motor pertama {$kumpulan_motor['Yafie']}";
echo "<br> Motor pertama {$kumpulan_motor['Ekky']}";
