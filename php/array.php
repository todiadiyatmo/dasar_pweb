<?php
// Deklarasi array 

$kumpulan_motor = array("Honda beat","Honda revo","Yamaha mio");

//melihat isi array , var_dump merupakan fungsi php yang berguna "membongkar isi variabel"

var_dump($kumpulan_motor);

//mengambil isi array satu per satu

echo "<br> Motor pertama {$kumpulan_motor[0]}";
echo "<br> Motor pertama {$kumpulan_motor[1]}";
echo "<br> Motor pertama {$kumpulan_motor[2]}";
