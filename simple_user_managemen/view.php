<?php 
require_once('config/database.php');

$query1 = ("SELECT * FROM users AS u JOIN (credentials as c) ON (u.id = c.user_id) ");
$result = mysql_query($query1);

while($row = mysql_fetch_array($result,MYSQL_ASSOC)){
	$data[] = $row;
}

?>

<!DOCTYPE HTML>
<html lang="id">
	<head>
		<title>Daftar User</title>
		<style>
			table {
				margin: 0 auto;
			}
			
			td {
				width: 200px;
				padding: 10px;
				vertical-align: top;
				background-color: #efefef;
			}
			
			th {
				padding: 10px;
				font-weight: bold;
				background-color: #9a9a9a;
			}
			
			td.alamat {
				width: 400px;
			}
			
			td.image {
				text-align: center;
			}
			
			td.image img{
				height: 100px
			}
			
			td.no {
				width: 20px;
				text-align: center;
			}
			
		</style>
	</head>
	<body>
		<table>
			<tr>
				<th>#</th>
				<th>Nama</th>
				<th>Alamat</th>
				<th>Email</th>
				<th>Photograph</th>
			</tr>
			<?php foreach($data as $key=>$value){ ?>
				<tr>
					<td class="no"><?php echo $key+1 ?></td>
					<td>
						<a alt="<?php echo $value['nama'] ?>" href="#" >
							<?php echo $value['nama'] ?>
						</a>
					</td>
					<td class="alamat"><?php echo $value['alamat'] ?></td>
					<td><?php echo $value['email'] ?></td>
					<td class="image"><img alt="<?php echo $value['nama'] ?>" src="<?php echo "files/{$value['file']}" ?>" /></td>
				</tr>
			<?php } ?>
		</table>
	</body>
</html>